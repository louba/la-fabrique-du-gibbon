Le format GIFT
(General Import Format Template)

// Question type
::La topologie des réseaux::
Quelle topologie de réseau est la plus résistante aux pannes et offre plusieurs chemins pour l'acheminement des données ?{
    ~La topologie en bus #Elle a un seul point de défaillance et n'offre qu'un seul chemin pour les données.
    ~La topologie en étoile #Elle dépend d'un nœud central, ce qui la rend vulnérable aux pannes de ce nœud. C'est celle qui est chez vous avec votre box.
    =Topologie maillée #Les terminaux sont reliés par plusieurs câbles ainsi si un est coupé le message peut passer par un autre chemin.
}

// Insérer un commentaire ici ou pas
::Nom de la question::
Quelle topologie de réseau est la plus résistante aux pannes et offre plusieurs chemins pour l'acheminement des données ?{
    ~Réponse fausse #Feedback
    ~Réponse fausse #Feedback
    =Réponse juste #Feedback
}

# Le bon prompt
Tu es professeur de sciences numériques et technologie pour des élève en seconde au lycée. Ta tache est rédiger 8 questions à choix multiples, vrai/faux ou questions de correspondance selon la taxonomie de Bloom portant sur le réseau internet : 
* le protocle IP : adresse d'expédition, adresse de destination.
* la requête, le client, le serveur, le datacenter
Les réponses fausses devront être proches des erreurs les plus probables et devront avoir un feedback permettant à l'élève de comprendre son erreur.
Tu formateras les réponses avec feedback directement en code au format GIFT.


// Insérer un commentaire ici ou pas
::[markdown]Calcul d'aire::
Calculer la surface d'un rectangle de 6 cm de largeur et 8 cm de longueur.{ ~56 #Feedback  ~14  #Feedback  =48 #Feedback} {=cm^2 ~cm ~cm^3}

# Caratère unicode
sur Bloc note Windows, taper Alt + code
Sur Gedit - Linux Ctrl + Shift + u et taper le code
Puissance 00B3 pour le 3
Puissance 2074 pour le 4
Puissance 2075 pour le 5
Puissance 2076 pour le 6
