import re
import openpyxl
from openpyxl.styles import Font

def parse_gift(gift_content):
    questions = []
    pattern = r'::(.+?)::\s*(.+?)\s*{(.+?)}'
    matches = re.findall(pattern, gift_content, re.DOTALL)
    
    for match in matches:
        title, question_text, answers_text = match
        # Modification du pattern pour ignorer les feedbacks (tout ce qui suit #)
        answers = re.findall(r'([=~])(.*?)(?=#|\Z)', answers_text, re.DOTALL)
        
        # Limiter à 4 réponses maximum
        answers = answers[:4]
        
        correct_answer = next((i + 1 for i, (prefix, _) in enumerate(answers) if prefix == '='), None)
        
        question = {
            'title': title.strip(),
            'question': question_text.strip(),
            'answers': [answer.strip() for _, answer in answers],
            'correct_answer': correct_answer
        }
        questions.append(question)
    
    return questions

def create_xlsx(questions, output_file):
    wb = openpyxl.Workbook()
    ws = wb.active
    ws.title = "Questions"

    headers = ["Question", "Réponse 1", "Réponse 2", "Réponse 3", "Réponse 4", "Temps de réponse maximum", "Numéro de la réponse juste"]
    for col, header in enumerate(headers, start=1):
        cell = ws.cell(row=1, column=col, value=header)
        cell.font = Font(bold=True)

    for row, question in enumerate(questions, start=2):
        ws.cell(row=row, column=1, value=f"{question['title']}\n\n{question['question']}")
        
        # Remplir les réponses disponibles (jusqu'à 4 max)
        for col, answer in enumerate(question['answers'], start=2):
            ws.cell(row=row, column=col, value=answer)
        
        # Remplir les cellules vides pour les réponses manquantes
        for col in range(len(question['answers']) + 2, 6):
            ws.cell(row=row, column=col, value="")
        
        ws.cell(row=row, column=6, value=60)  # Temps de réponse par défaut
        ws.cell(row=row, column=7, value=question['correct_answer'])

    # Ajuster la largeur des colonnes
    for col in ws.columns:
        max_length = 0
        column = col[0].column_letter
        for cell in col:
            if len(str(cell.value)) > max_length:
                max_length = len(str(cell.value))
        adjusted_width = (max_length + 2)
        ws.column_dimensions[column].width = adjusted_width

    wb.save(output_file)

# Lecture du fichier GIFT
try:
    with open('questions-gift.txt', 'r', encoding='utf-8') as file:
        gift_content = file.read()
except FileNotFoundError:
    print("Erreur : Le fichier 'questions-gift.txt' n'a pas été trouvé.")
    exit(1)
except IOError:
    print("Erreur : Impossible de lire le fichier 'questions-gift.txt'.")
    exit(1)

# Traitement et création du fichier Excel
questions = parse_gift(gift_content)
create_xlsx(questions, "questions_qcm.xlsx")
print("Le fichier Excel 'questions_qcm.xlsx' a été créé avec succès.")
