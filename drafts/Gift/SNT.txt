// Historique
::L'histoire d'Internet - les débuts::
Quelle était la première version d'Internet, développée par le département de la Défense des États-Unis dans les années 1960 ?{
    =ARPANET
    #WorldWideWeb est en fait le nom du premier navigateur web, créé par Tim Berners-Lee en 1990.
    #NSFNET a été créé plus tard, dans les années 1980, pour connecter les universités américaines.
    #MILNET était une partie d'ARPANET qui a été séparée pour un usage militaire en 1983.
}

::L'histoire d'Internet - le web::
En quelle année à été le premier site web été mis en ligne ?{
    =1991 #Tim Bernes Lee présente son projet le 6 août 1991 et il est accessible à cette adresse http://info.cern.ch/hypertext/WWW/TheProject.html
    ~2000 #Non, c'est le début du Web 2.0, une interaction du site avec l'utilisateur.
    ~1969 #C'est la première fois qu'on met réseau des ordinateurs, c'est ARPANET
    ~1958 #Création de l'ARPA : agence pour les projets de recherche avancée.
}

::L'histoire d'Internet - Spoutnik::
En quelle année le 1er satellite a été envoyé dans l'espace par les russes ?{
    ~1991 #Tim Bernes Lee présente son projet le 6 août 1991 : le World Wide Web.
    ~2000 #Non, c'est le début du Web 2.0, une interaction du site avec l'utilisateur.
    ~1969 #C'est la première fois qu'on met réseau des ordinateurs, c'est ARPANET
    =1957 #Bravo et en réaction les américains crée de l'ARPA pour rattraper leur retard.
}

::L'histoire d'Internet - Internet à ses débuts::
Comment s'appelle le premier réseau d'ordinateurs ?{
    ~Web #Tim Bernes Lee présente le World Wide Web : des pages accessibles via un navigateur.
    ~Web 2.0 #Non, c'est le début du Web 2.0, une interaction du site avec l'utilisateur.
    ~ARPA #En réaction au lancement de Spoutnik par les russes,les américains crée de l'ARPA pour rattraper leur retard.
    =ARPANET #Bravo, C'est la première fois qu'on met réseau des ordinateurs en 1969.
}

// Topologie des réseaux
::La topologie des réseaux::
Quelle topologie de réseau est la plus résistante aux pannes et offre plusieurs chemins pour l'acheminement des données ?{
    ~La topologie en bus #Elle a un seul point de défaillance et n'offre qu'un seul chemin pour les données.
    ~La topologie en étoile #Elle dépend d'un nœud central, ce qui la rend vulnérable aux pannes de ce nœud. C'est celle qui est chez vous avec votre box.
    =Topologie maillée #Les terminaux sont reliés par plusieurs câbles ainsi si un est coupé le message peut passer par un autre chemin.
}

// Question 3
::Le codage de l'information::
Combien d'octets sont nécessaires pour représenter le texte "Internet" en utilisant le codage ASCII standard ?{
    ~7 octets#7 octets serait correct si on utilisait seulement 7 bits par caractère, mais l'ASCII standard utilise 8 bits.
    =8 octets
    ~16 octets#16 octets serait correct si on utilisait un codage sur 16 bits comme UTF-16, mais ce n'est pas le cas avec l'ASCII standard.
    ~32 octets#32 octets serait beaucoup trop pour ce mot en ASCII standard.
}

// Question 4
::Le protocole IP::
Analysez cette adresse IP : 192.168.1.1. À quelle classe d'adresse IP appartient-elle ?{
    ~Classe A#Les adresses de classe A commencent par un nombre entre 1 et 126.
    ~Classe B#Les adresses de classe B commencent par un nombre entre 128 et 191.
    =Classe C
    ~Classe D#Les adresses de classe D sont utilisées pour le multicast et commencent par un nombre entre 224 et 239.
}

// Question 5
::Le protocole TCP::
Évaluez l'importance du protocole TCP dans le transfert de données sur Internet. Quelle affirmation est correcte ?{
    ~TCP assure uniquement la rapidité de la transmission des données.#Bien que TCP puisse optimiser la vitesse, sa principale fonction est la fiabilité, pas la rapidité.
    =TCP garantit la livraison des paquets dans l'ordre et sans erreur.
    ~TCP est responsable du routage des paquets à travers le réseau.#Le routage des paquets est la responsabilité du protocole IP, pas de TCP.
    ~TCP détermine l'adresse IP de destination des paquets.#La détermination de l'adresse IP de destination est également une fonction du protocole IP, pas de TCP.
}

// Question 6
::Acheminement de l'information::
Créez un scénario où l'utilisation du protocole de routage dynamique serait préférable au routage statique.{
    ~Un petit réseau domestique avec 3 ordinateurs connectés à un routeur.#Un petit réseau domestique est trop simple pour nécessiter un routage dynamique.
    =Une entreprise avec plusieurs succursales dans différentes villes qui échangent fréquemment des données.
    ~Un café Internet avec 10 ordinateurs connectés en réseau local.#Un café Internet local n'a généralement pas besoin de routage complexe.
    ~Une école primaire avec un laboratoire informatique de 20 ordinateurs.#Une école primaire avec un seul laboratoire informatique n'a pas besoin de routage dynamique pour son réseau local.
}

// Question 7
::La structure du réseau::
Quel élément du réseau Internet est responsable de la traduction des noms de domaine en adresses IP ?{
    ~Le routeur#Le routeur est responsable de l'acheminement des paquets entre différents réseaux.
    ~Le commutateur (switch)#Le commutateur gère le trafic au sein d'un réseau local.
    =Le serveur DNS
    ~Le modem#Le modem convertit les signaux numériques en signaux analogiques et vice versa pour la transmission sur les lignes téléphoniques.
}
