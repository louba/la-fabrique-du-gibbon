
Accessoires
Bureautique
Infographie
Internet
Outils système
Programmation
Sciences
Son et vidéo
Utilitaires
# logiciels sur ordinateur
VLC
Celluloid
Gimp
Inkscape
LibreOffice
Logseq
Syncthing
Keepass XC
VsCodium
Firefox avec Addons
Speech Note
Yt-dl ou freetube
OBS
Handbrake
gnome-shell-extension-manager
gnome-tweaks
tor Browser
yt-dlp 
git

# Apps sur smartphone
Mon OS : 
Antennapod
Camscanner
Client Bureau à distance Microsoft
K9-mail
DavX5
F-Droid
Far Commander
Firefox avec Ublock-origin
Forecast
Jami
Keepass2Android
LocalSend
Logseq
Mastodon
MJ PDF
Collabora
NewPipe
Ning
personalDNSfilter
PlantNet
Read You
Simple calendar Pro
SyncThing
UsageDirect ou Screen Time
QKsms


Commandes
sudo apt update
sudo apt upgrade
sudo apt install flatpak
sudo apt install gnome-software-plugin-flatpak
flatpak remote-add --if-not-exists flathub https://flathub.org/repo/flathub.flatpakrepo

# Installer gnome extension manager icône bleu
sudo apt install gnome-shell-extension-manager

# Installer vlc et les codecs
sudo apt install libdvdread8t64 libavcodec-extra vlc vlc-l10n

```bash
#!/bin/bash

# Mettre à jour le système
sudo apt update && sudo apt upgrade -y

# Installer les dépendances nécessaires
sudo apt install -y flatpak gnome-software-plugin-flatpak

# Ajouter le dépôt Flathub
flatpak remote-add --if-not-exists flathub https://flathub.org/repo/flathub.flatpakrepo

# Fonction pour installer des paquets
install_package() {
    if ! dpkg -s "$1" >/dev/null 2>&1; then
        sudo apt install -y "$1"
    else
        echo "$1 est déjà installé."
    fi
}

# Fonction pour installer des Flatpaks
install_flatpak() {
    if ! flatpak list | grep -q "$1"; then
        flatpak install -y flathub "$1"
    else
        echo "$1 (Flatpak) est déjà installé."
    fi
}

# Installer les applications
install_package vlc
install_package adwaita-qt #Dark mode pour vlc
install_package libdvd-pkg # Lire un DVD
install_package celluloid
install_package gimp
install_package inkscape
install_package firefox
install_package obs-studio
install_package handbrake
install_package gnome-shell-extension-manager
install_package gnome-tweaks
install_package yt-dlp
install_package lame asunder
install_package git


install_flatpak com.github.marktext.marktext
install_flatpak com.github.zocker_160.SyncThingy
install_flatpak com.logseq.Logseq
install_flatpak com.mattjakeman.ExtensionManager
install_flatpak com.nextcloud.desktopclient.nextcloud
install_flatpak com.vscodium.codium
install_flatpak com.wps.Office
install_flatpak io.github.ungoogled_software.ungoogled_chromium
install_flatpak io.github.ungoogled_software.ungoogled_chromium.Codecs
install_flatpak net.mkiol.SpeechNote
install_flatpak net.mkiol.SpeechNote.Addon.nvidia
install_flatpak io.gitlab.theevilskeleton.Upscaler
install_flatpak net.xmind.XMind
install_flatpak org.gtk.Gtk3theme.Yaru-dark
install_flatpak org.kde.ghostwriter
install_flatpak org.keepassxc.KeePassXC
install_flatpak org.libreoffice.LibreOffice
install_flatpak org.onlyoffice.desktopeditors
install_flatpak org.torproject.torbrowser-launcher


# Installer des add-ons Firefox populaires (à personnaliser selon vos besoins)
firefox --headless https://addons.mozilla.org/firefox/downloads/file/3933192/ublock_origin-1.52.2.xpi
firefox --headless https://addons.mozilla.org/firefox/downloads/file/4003969/privacy_badger-2023.6.13.1.xpi

echo "Installation terminée !"
```


 Liens vers les sites No-prism


