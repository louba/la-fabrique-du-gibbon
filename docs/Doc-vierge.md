---
title: Ecrire en Markdown
summary: A brief description of my document.
authors:
    - Vincent
date:
#   created: 2023-12-31
#   updated: 2024-01-02
  icon: material/home
  locale: fr
---

# Comment en écrire en Markdown pour site MkDocs?
## Syntaxe de base
### Titres

Voici le premier paragraphe.


=== "Markdown"
Le titre de niveau 1 est le titre de l'article[^1].
    ```
        # Titre de niveau 1
        ## Titre de niveau 2
        ### Titre de niveau 3
        #### Titre de niveau 4
    ```


### Styles du texte

le texte peut en **gras** `**gras**` ou en *ITALIQUE* `*italique*`

### Liens
=== "Hypertexte | Rendu"
    Un lien hypertexte [Lien](https://wikipedia.fr)
=== "Hypertexte | Markdown"
    ```
        Un lien hypertexte [Lien](https://wikipedia.fr)
    ```
=== "Article | Rendu"
    Vous pouvez aller voir cet [article](../mon_doc.md).
=== "Article | Rendu"
    ```
    Vous pouvez aller voir cet [article](../mon_doc.md)
    ```
=== "Titre | Rendu"
    Un lien titre [Les tableaux](#les-tableaux)
=== "Titre | Markdown"
    ```
        Un lien titre [Les tableaux](#les-tableaux)
    ```
### Insertion d'image
=== "Rendu"
    Insertion d'une image ![Lien vers](https://fr.wikipedia.org/static/images/icons/wikipedia.png)
=== "Markdown"
    ```
    Insertion d'une image ![Lien vers](https://fr.wikipedia.org/static/images/icons/wikipedia.png)
    ```


### Listes
=== " Puce | Rendu"

    - Sed sagittis eleifend rutrum
    - Donec vitae suscipit est
    - Nulla tempor lobortis orci
=== "Puce | Markdown"
    ```  
        - Sed sagittis eleifend rutrum
        - Donec vitae suscipit est
        - Nulla tempor lobortis orci
    ```
=== "Numérotées - Rendu"

    1. Sed sagittis eleifend rutrum
    2. Donec vitae suscipit est
    3. Nulla tempor lobortis orci
=== "Numérotées | Markdown"

    ```
        1. Sed sagittis eleifend rutrum
        2. Donec vitae suscipit est
        3. Nulla tempor lobortis orci
    ```


## Avertissements - admonitions
### Liste
- note
- abstract "Résumé"
- info
- tip "Astuce"
- success
- question 
- warning  "Remarque"
- failure "Echec"
- danger "Attention"
- bug
- example
- quote "Citation"

### Syntaxe
<!-- Avertissement avec titre -->
=== "Avertissement avec titre | Rendu"
    !!! note "Nota Bene" 
        Contenu des avertissements
=== "Avertissement avec titre | Markdown"
    ```
        !!! note "Nota Bene" 
            Contenu des avertissements
    ```

<!-- Avertissement sans titre -->	
=== "Avertissement sans titre | Rendu"
    !!! abstract "" 
        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla et euismod
        nulla. Curabitur feugiat, tortor non consequat finibus, justo purus auctor
        massa, nec semper lorem quam in massa.
=== "Avertissement sans titre | Markdown"
    ```
        !!! abstract "" 
            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla et euismod
            nulla. Curabitur feugiat, tortor non consequat finibus, justo purus auctor
            massa, nec semper lorem quam in massa.
    ```
 
<!-- Avertissement replié -->
=== "Avertissement replié | Rendu"
    ??? tip "Astuce"
        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla et euismod
        nulla. Curabitur feugiat, tortor non consequat finibus, justo purus auctor
        massa, nec semper lorem quam in massa.
=== "Avertissement replié | Markdown"
    ```
        ??? tip "Astuce"
            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla et euismod
            nulla. Curabitur feugiat, tortor non consequat finibus, justo purus auctor
            massa, nec semper lorem quam in massa.
    ```
<!-- Avertissement repliable -->
=== "Avertissement repliable | Rendu"
    ???+ danger "Attention"
        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla et euismod
        nulla. Curabitur feugiat, tortor non consequat finibus, justo purus auctor
        massa, nec semper lorem quam in massa.
        peut
=== "Avertissement repliable | Markdown"
    ```
    ???+ danger "Attention"
        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla et euismod
        nulla. Curabitur feugiat, tortor non consequat finibus, justo purus auctor
        massa, nec semper lorem quam in massa.
        peut
    ```



## Les tableaux

=== "Rendu"

    First Header | Second Header | Third Header
    :----------- |:-------------:| -----------:
    Left         | Center        | Right
    Left         | Center        | Right

=== "Markdown"

    ```
        First Header | Second Header | Third Header
        :----------- |:-------------:| -----------:
        Left         | Center        | Right
        Left         | Center        | Right
    ```
## Graphes
```mermaid
    graph LR
        A[Fichier] --> D((D))
        D --> C[Clavier]
        C --> B((B))
        B --> A
```
## Commentaires
```
<!-- This content will not appear in the rendered Markdown -->
[comment]: # (Mon commentaire)
Commentaire avec VSCode : Ctrl + /
```
## Pour aller plus loin
[www.linuxadictos.com et le Markdown](https://www.linuxadictos.com/fr/Le-d%C3%A9marque-est-probablement-le-meilleur-moyen-de-prendre-des-notes-personnelles%2C-nous-vous-expliquons-comment-l%27utiliser.html)
[^1]: Note de bas de page  
[Pour plus d'explications ou d'exemples](https://docs.forge.apps.education.fr/tutoriels/pyodide-mkdocs-theme-review/02_basique/2_page_basique/#ix-ecriture-de-code-complements)