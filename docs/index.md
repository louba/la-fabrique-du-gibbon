---
title: Gibbon | Accueil
icon: material/home 
date: 2024-08-06
---
<figure markdown="span">
    ![image](images/gibbon.png/)
</figure>
# Qui suis-je ?  
Je suis un professeur en sciences de l'ingénieur, mon cœur de métier est la physique du bâtiment ou comment concevoir un bâtiment durable ? J'enseigne en lycée et mes centres d'intérêt sont : 

- le bâtiment,  
- les ateliers de fabrication de type Tiers-Lab ou FabLab,  
- la pédagogie et la didactique liées notamment aux sciences cognitives,  
- la protection de la vie privée,  
- la construction de la connaissance et les mécanisme de prises de décision.  
En somme, j'aime desconstruire pour reconstruire et parfois cassé.

## Pourquoi un espace de partage ? 
La réponse est dans la question. C'est en partageant des connaissances qu'on en acquiert de nouvelles. C'est également un défi pour moi de publier dans une forge logiciels car je n'y comprend encore pas grand chose.

L'objectif est de faire une veille sur l'ensemble des sujets cités, ainsi je tenterai de mettre à jour au fur et à mesure les pages.  

!!! danger "Attention" 
    Certaines pages sont en cours de rédaction donc la cohérence peut parfois être altérée. :material-emoticon-confused: Bonne chance. 

Allez c'est parti !
