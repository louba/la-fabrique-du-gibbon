---
title: Le prompt qui crée des QUIZ
summary: Comment générer un bon prompt pour l'IA qui crée des QCM.
authors:
    - Vincent
date:
#   created: 2023-12-31
#   updated: 2024-01-02
  icon: 
  locale: fr
---

# Comment créer des questionnaires pour Moodle avec une IA ?
## Le prompt

=== "Le prompt pour claude.ia ou ChatGPT"
    Voici un prompt court qui fonctionne pour les IA contenant un grand nombre de paramètres comme [Claude.ai](https://claude.ai/) ou [ChatGPT](https://chatgpt.com/).
    
    >Tu es professeur de mathématiques pour des élèves en seconde au lycée. Ta tâche est de rédiger 8 questions à choix multiples selon la taxonomie de Bloom portant sur les conversions d'unités.  
    Les réponses fausses devront être proches des erreurs les plus probables et devront avoir un feedback permettant à l'élève de comprendre son erreur.  
    Tu formateras les réponses avec feedback directement en code au format GIFT.
    
=== "Le prompt pour Mistral.ai"
    Voici un prompt plus long qui fonctionne pour les IA contenant un nombre de paramètres restreint [Mistral.ai](https://chat.mistral.ai/chat) ou celles que l'on installe sur son PC avec le logiciel [ollama](https://ollama.com).
    
    >Tu es professeur de mathématiques pour des élèves en seconde au lycée. Ta tâche est de rédiger 8 questions à choix multiples selon la taxonomie de Bloom portant sur les conversions d'unités.  
    Les réponses fausses devront être proches des erreurs les plus probables et devront avoir un feedback permettant à l'élève de comprendre son erreur.   
    Tu formateras les réponses avec feedback directement en code au format GIFT.  
    Voici un exemple de question:  
    // Question 4 : Analyse    
    ::Q4 Analyse d'une conversion erronée::    
    Un élève convertit 5 m²/s en cm²/min et obtient 30000 cm²/min. Quelle erreur a-t-il probablement commise ?   
    {   
        =Il a oublié de multiplier par 60 pour convertir les secondes en minutes #Correct ! 5 m²/s = 50000 cm²/s = 3000000 cm²/min.     
        ~Il a multiplié par 100 au lieu de 10000 pour passer de m² à cm² #Ce n'est pas l'erreur principale. Vérifiez la conversion du temps.   
        ~Il a divisé par 60 au lieu de multiplier #Cette erreur donnerait un résultat beaucoup plus petit. Réfléchissez à la conversion du temps.  
        ~Il n'a pas fait d'erreur, le résultat est correct #Le résultat est incorrect. Analysez attentivement les étapes de conversion.  
    }  
    
## Variations

>Tu es professeur de mathématiques pour des élèves en seconde au lycée. Ta tâche est de rédiger 8 questions à choix multiples, **vrai/faux ou questions de correspondance** selon la taxonomie de Bloom portant sur les conversions d'unités.
Les réponses fausses devront être proches des erreurs les plus probables et devront avoir un feedback permettant à l'élève de comprendre son erreur.
Tu formateras les réponses avec feedback directement en code au format GIFT **et tu insèreras la catégorie avec le formatage suivant : $CATEGORY: Mathématiques/Conversions**


## Importer dans la banque aux questions
Enregistrer le résulat dans un fichier txt avec le Bloc-notes.

## Convertir les questions pour les importer dans Kahoot
Le format d'import de questions pour Kahoot est un fichier tableur  xlsx. avec :

- Colonne 1 : la question
- colonne 2 : réponse 1
- colonne 3 : réponse 2
- colonne 4 : réponse 3
- colonne 5 : réponse 4
- colonne 6 : Temps de réponse maximum
- Colonne 7 : numéro de la réponse juste.

Il est possible de prendre le fichier GIFT est de le convertir avec un script Python au format xlsx. Il faudra avoir le logiciel Python et la bibliothèque `openpyxl` installés.
Le fichier GIFT devra contenir uniquement des questions à choix multiples sans poids `%poids%`,sans vrai/faux et sans correspondances. Le fichier GIFT doit être renommé en `questions-gift.txt`. Placer le fichier dans le même dossier que le script et exécuter le script.

Pour exécuter le script :
```bash
python3 NomDuFichierPython.py
```

```python
import re
import openpyxl
from openpyxl.styles import Font

def parse_gift(gift_content):
    questions = []
    pattern = r'::(.+?)::\s*(.+?)\s*{(.+?)}'
    matches = re.findall(pattern, gift_content, re.DOTALL)
    
    for match in matches:
        title, question_text, answers_text = match
        # Modification du pattern pour ignorer les feedbacks (tout ce qui suit #)
        answers = re.findall(r'([=~])(.*?)(?=#|\Z)', answers_text, re.DOTALL)
        
        # Limiter à 4 réponses maximum
        answers = answers[:4]
        
        correct_answer = next((i + 1 for i, (prefix, _) in enumerate(answers) if prefix == '='), None)
        
        question = {
            'title': title.strip(),
            'question': question_text.strip(),
            'answers': [answer.strip() for _, answer in answers],
            'correct_answer': correct_answer
        }
        questions.append(question)
    
    return questions

def create_xlsx(questions, output_file):
    wb = openpyxl.Workbook()
    ws = wb.active
    ws.title = "Questions"

    headers = ["Question", "Réponse 1", "Réponse 2", "Réponse 3", "Réponse 4", "Temps de réponse maximum", "Numéro de la réponse juste"]
    for col, header in enumerate(headers, start=1):
        cell = ws.cell(row=1, column=col, value=header)
        cell.font = Font(bold=True)

    for row, question in enumerate(questions, start=2):
        ws.cell(row=row, column=1, value=f"{question['title']}\n\n{question['question']}")
        
        # Remplir les réponses disponibles (jusqu'à 4 max)
        for col, answer in enumerate(question['answers'], start=2):
            ws.cell(row=row, column=col, value=answer)
        
        # Remplir les cellules vides pour les réponses manquantes
        for col in range(len(question['answers']) + 2, 6):
            ws.cell(row=row, column=col, value="")
        
        ws.cell(row=row, column=6, value=60)  # Temps de réponse par défaut
        ws.cell(row=row, column=7, value=question['correct_answer'])

    # Ajuster la largeur des colonnes
    for col in ws.columns:
        max_length = 0
        column = col[0].column_letter
        for cell in col:
            if len(str(cell.value)) > max_length:
                max_length = len(str(cell.value))
        adjusted_width = (max_length + 2)
        ws.column_dimensions[column].width = adjusted_width

    wb.save(output_file)

# Lecture du fichier GIFT
try:
    with open('questions-gift.txt', 'r', encoding='utf-8') as file:
        gift_content = file.read()
except FileNotFoundError:
    print("Erreur : Le fichier 'questions-gift.txt' n'a pas été trouvé.")
    exit(1)
except IOError:
    print("Erreur : Impossible de lire le fichier 'questions-gift.txt'.")
    exit(1)

# Traitement et création du fichier Excel
questions = parse_gift(gift_content)
create_xlsx(questions, "questions_qcm.xlsx")
print("Le fichier Excel 'questions_qcm.xlsx' a été créé avec succès.")
```

## Créer des flashcards avec Anki
Un fichier .txt composé de tablulations comme un fichier csv composé de trois colonnes :

- La question
- La réponse
- Les tags, étiquettes séparées par des espaces

```anki.txt
separator:tab
#html:true
#tags column:3
Question 1	Reponse 1	Etiquette1 Etiquette3 Etiquettte2
Question 2	Reponse 2	Etiquette1 Etiquette3 Etiquettte2
```
