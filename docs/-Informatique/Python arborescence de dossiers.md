---
title: Markdown -> Arborescence
summary: Comment créer un arborescence de dossiers à partir d'un fichier markdoewn.
authors:
    - Vincent
date:
#   created: 2023-12-31
#   updated: 2024-01-02
  icon: material/home
  locale: fr
---
# Comment créer une arborescence de dossiers  à partir d'une carte mentale
Je présente un petit script pour générer une arborescence de dossiers à partir d'un fichier Markdown.
```markdown
# 01_Animaux
## 01_01_Invertebres
### 01_01_01_Mollusques
### 01_01_02_Vers
## 01_02_Vertebres
### 01_02_01_Mammiferes
### 01_02_02_Oiseaux
### 01_02_03_Reptiles
```
1. Créer dossier nommé Python_Creer_arborescence.  
2. Copier le code dans un fichier texte et le renommer structure.md  
3. Installer [python](hhttps://fr.wikihow.com/installer-Python) sur votre ordinateur.  

``` python
import os

def create_directories_from_markdown(markdown_file):
    with open(markdown_file, 'r') as file:
        lines = file.readlines()

    base_path = "output"
    os.makedirs(base_path, exist_ok=True)
    current_path = base_path

    for line in lines:
        line = line.strip()
        if line.startswith('#'):
            level = line.count('#')
            folder_name = line.replace('#', '').strip()
            path_parts = current_path.split(os.sep)[:level]
            current_path = os.path.join(*path_parts, folder_name)
            os.makedirs(current_path, exist_ok=True)

if __name__ == "__main__":
    markdown_file = 'structure.md'  # Nom de votre fichier Markdown
    create_directories_from_markdown(markdown_file)
```
4. Copier le code ci-dessus dans fichier nommé Creer_Arborescence.py Les deux fichiers sont dans le même dossier.  
5. Exécuter-le en tapant la commande `Python Creer_Arborescence.py`

