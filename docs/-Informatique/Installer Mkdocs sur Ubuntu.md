---
title: Mkdocs sur Ubuntu
summary: Comment installer MkDocs sur Ubuntu Linu. Tutoriel
authors:
    - Vincent
date:
#   created: 2023-12-31
#   updated: 2024-01-02
  icon: material/home
  locale: fr
---
# Comment installer MkDocs sur Ubuntu Linux avec pipx ?

MkDocs est un générateur de site statique rapide semblable à [Docusaurus](https://docusaurus.io/), [Sphynx](https://www.sphinx-doc.org/fr) ou même [Hugo](https://gohugo.io/) simple qui construit entièrement votre documentation à partir de fichiers Markdown. Dans ce tutoriel, nous allons voir comment l'installer sur Ubuntu en utilisant pipx, ainsi que l'ajout de plugins populaires.

## Prérequis

Avant de commencer, assurez-vous d'avoir Python 3.6 ou ultérieur installé sur votre système Ubuntu Linux.

## Étape 1 : Installer pipx

Pipx est un outil pratique pour installer et exécuter des applications Python dans des environnements virtuels isolés. Exécutez la commande suivante pour l'installer :

```bash
sudo apt update
sudo apt install pipx
pipx ensurepath
```

## Étape 2 : Installer MkDocs avec pipx

Exécutez cette commande pour installer MkDocs à l'aide de pipx :

```bash
pipx install mkdocs
```

## Étape 3 : Installer des plugins MkDocs

Nous allons maintenant ajouter deux plugins populaires à notre installation MkDocs :

1. **[mkdocs-material](https://squidfunk.github.io/mkdocs-material/)** : Un thème Material Design très populaire pour MkDocs.
2. **[mkdocs-git-revision-date-localized-plugin](https://github.com/timvink/mkdocs-git-revision-date-localized-plugin)** : joute la date de la dernière modification Git au pied de page.
3. **[mkdocs-rss-plugin](https://github.com/guts/mkdocs-rss-plugin)** : génère un fichier xml contenant l'ensemble des derniers documents modifiés ou créés (au choix).


Exécutez ces commandes pour les installer :

```bash
pipx inject mkdocs mkdocs-material
pipx inject mkdocs mkdocs-git-revision-date-localized-plugin
pipx inject mkdocs mkdocs-rss-plugin

```

## Étape 4 : Créer un nouveau projet MkDocs

Naviguez vers le répertoire où vous souhaitez créer votre projet MkDocs et exécutez :

```bash
mkdocs new NomDuSiteWeb
```

Cela créera un fichier de configuration nommé `mkdocs.yml` et un répertoire `docs` contenant un fichier `index.md`.
```
.
├─ docs/
│  └─ index.md
└─ mkdocs.yml
```

## Étape 5 : Prévisualiser le site

Pour prévisualiser votre site MkDocs avec le thème Material installé, exécutez :

```bash
cd NomDuSiteWeb
mkdocs serve
```
Vous devriez maintenant pouvoir accéder à http://127.0.0.1:8000/ dans votre navigateur pour voir votre site MkDocs !

Le thème **Material** n'est pas pris en charge et c'est normal !

## Etape 6 : Modifier le thème thème et ajouter les extensions et les plugins

Editez le fichier `mkdocs.yml`  
Supprimez son contenu et coller le suivant :
!!! danger "Attention"  
      Les indentations (retrait) sont importantes pour l'interprétation du fichier.  

??? abstract "Cliquez pour visualiser le code" 
    ```yaml
    site_name: Le nom de votre site
    site_url: https://monsite.fr
    site_description: blabla 
    theme:
      icon:
        repo: fontawesome/brands/git-alt # mettre l'icone dans le lien vers gitlab  ou github droite
      name: material # nom du thème
      language: fr #Langue du thème - navigation pied de page - suivant
      font: false # Supprimer l'appel des polices Google
      logo: assets/logo.svg # Le dossiser assets est situé dans le dossier docs
      favicon: images/favicon.ico # Image contenu dans l'onglet du navigateur. Le dossier image est situé dans le dossier docs
      features:
        - content.code.copy # Bouton de copie du code dans un bloc Code
        - navigation.footer # Navigation dans le pied de page
        - toc.follow # Faire suivre la table des matières avec le scroll

      palette: # Pour choisir son thème https://squidfunk.github.io/mkdocs-material/setup/changing-the-colors/
        # Palette toggle for automatic mode
        - media: "(prefers-color-scheme)"
          toggle:
            icon: material/brightness-auto
            name: Basculer en style clair

        # Palette toggle for light mode
        - media: "(prefers-color-scheme: light)"
          scheme: default 
          primary: purple
          toggle:
            icon: material/brightness-7
            name: Basculer en style sombre

        # Palette toggle for dark mode
        - media: "(prefers-color-scheme: dark)"
          scheme: slate
          primary: teal
          toggle:
            icon: material/brightness-4
            name: Basculer selon les préferences du système

    markdown_extensions:
      - pymdownx.superfences: #imbrication arbitraire de codes et de blocs de contenu les uns à l'intérieur des autres, y compris les admonestations, les onglets, les listes
          custom_fences: # Affichage des graphes grâce à Mermaid
            - name: mermaid
              class: mermaid
              format: !!python/name:pymdownx.superfences.fence_code_format
      - admonition #Avertissement Note, abstract, info, tip, success, etc.
      - pymdownx.details #Avertissement avec repliage (collapsible)
      - pymdownx.tabbed:
          alternate_style: true #Boite à onglets
      - footnotes # Ajouter les notes de bas de page
      - md_in_html # Pour intégrer du html, exemple : des images centrées 
      - toc:
          permalink: true # Lien vers la section, caratère en fin de titre.
          permalink_title: Lien vers la section # Texte affcihé au survol
      - pymdownx.emoji: # Affichage des Emojis - Choisir un Emoji https://squidfunk.github.io/mkdocs-material/reference/icons-emojis/
          emoji_index: !!python/name:material.extensions.emoji.twemoji
          emoji_generator: !!python/name:material.extensions.emoji.to_svg

    plugins:
      # - git-revision-date-localized: #Date de création et mise à jour du fichier selon git en bas de page
      #     locale: fr
      #     type: timeago
      #     custom_format: "%d/%B/%Y"
      #     timezone: Europe/Paris
      #     fallback_to_build_date: false #Permet de retomber au moment où mkdocs builda été exécuté lorsque git n'est pas disponible
      #     enable_creation_date: true # Ajout de la date de création
      #     enabled: true #  Vous permet de désactiver ce plugin.
      #     strict: true # Avec rapport d'erreur
      - search # Ajout de la barre de recherche en haut à droite
      - rss # Appel du plugin flux rss

        
    repo_url: https://monprojet.gitlab.com # mettre le lien vers gitlab  header droite
    repo_name: La Forge logiciellle
    copyright: Ce travail est sous licence CC BY 4.0  # Phrase du copyright en bas de page
      
    extra:
      generator: false # Supprime le texte "Made with Material for MkDocs" dans le pied de page
      social: # Ajout des icones avec lien vers une chaine peertube et le flux rss en pied de page.
        - icon: fontawesome/brands/youtube
          link: https://tube-numerique-educatif.apps.education.fr/c/outils_numeriques
          name: Yalb | Outils Numériques
        - icon: fontawesome/solid/square-rss
          link: /nomDuDepot/feed_rss_updated.xml
          name: Flux rss

    ```
    !!! abstract "Info"
        Les lignes précédées du caractère `#` sont commentées.Elles correspondent à la partie *Date de création et de révision de l'article*. Ces pourront être dé-commentées après le premier `git add .`

## Etape 7 : Créer le site
Il existe deux possibilités :
- héberger l'ensemble des fichiers sur un logiciel de forge de type gitlab ou github, les fichiers html, css et javascript sont générées directement sur la plateforme,
- héberger sur un serveur web de type NGINX ou ApacheHTTP, ilfaut alors créer les fichiers html, css et javascript sur votre machine.
=== "Serveur web"
    Ce serveur web peut être des pages perso free ou un rasberry pi chez vous.
    Lancez la commande suivante : 
    ```
    mkdocs build
    ```
    Un dossier `site` sera crée. Ouvrez le fichier index.html avec un navigateur pour pouvoir accès au site.
    !!! abstract "Info"
        Les liens ne seront pas effectifs car vous n'êtes pas sur un serveur web. Il faudra cliquer sur index.html à chaque lien ouvert.

    Ensuite téléversez les fichiers sur le serveur web.

=== " Logiciel de forge"
    Créer un compte chez gitlab, github ou un autre.
    Je vous invite à regarder un tutoriel expliquant la démarche. Si votre première fois je vous invite à [cloner un site avec ce tutoriel](https://docs.forge.apps.education.fr/tutoriels/tutoriel-site-basique/08_tuto_fork/1_fork_projet/) pour vous entrainer.
    
    Il faudra ajouter pour gitlab un fichier nommé `.gitlab-ci.yml` avec le contenu suivant :

    ```
      pages:
      stage: deploy
      image: python:latest
      script: # ici on ajoute les plugins essentiels que l'on a utilisé précédement
        - pip install mkdocs-material
        - pip install mkdocs-git-revision-date-localized-plugin
        - pip install mkdocs-autorefs
        - pip install mkdocs-rss-plugin

        - mkdocs build --site-dir public # On crée les fichiers html, css et javascript, le rendu du site que l'on place dans le dossier nommé public
      artifacts:
        paths:
          - public
      rules:
        - if: '$CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH'
    ```



## Conclusion

Vous avez maintenant installé MkDocs sur Ubuntu avec pipx et ajouté des plugins populaires. Vous pouvez commencer à créer et personnaliser votre documentation à l'aide de fichiers Markdown situés dans le dossier `docs`. N'hésitez pas à explorer davantage les fonctionnalités et les paramètres de MkDocs et de ses plugins.