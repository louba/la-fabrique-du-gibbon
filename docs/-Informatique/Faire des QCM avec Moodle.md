---
title: QCM - Moodle
summary: Comment concevoir des QCM, QUIZ avec Moodle et le format GIFT ?
authors:
    - Vincent
date:
#   created: 2023-12-31
#   updated: 2024-01-02
  icon: material/home
  locale: fr
---
# Comment concevoir des QCM, QUIZ avec Moodle et le format GIFT ?

Le format GIFT signfie General Import Format Template.

!!! note ""
    La page est une traduction partielle en français de la [page originale](https://docs.moodle.org/404/en/GIFT_format) de la documentation Moodle.

## Instructions générales
Au moins une **ligne vide** doit être laissée entre chaque question.

Dans la forme simple, la question vient en premier, puis les réponses sont placées entre parenthèses `{}`:  

- avec un signe égal (=) indiquant la ou les bonnes réponses   
- un tilde (~) les mauvaises réponses,  
- un hashtag (#) insérera un feedback.   
- Les questions peuvent être pondérées en plaçant des signes de pourcentage (%..%) autour du poids.   
- Les commentaires sont précédés de doubles barres obliques (//) et ne sont pas importés.  

Voici quelques exemples de GIFT utiles qui peuvent être importés ou utilisés comme modèle approximatif. La plupart des exemples ci-dessous ont utilisé les questions du dossier comme point de départ.

### Formater les symboles

Voici quelques symboles courants et leur utilisation.

  ---
| Symboles |                          Utiliser|
|:-------:|:---------------------------------|
| // texte|                           Commentaire jusqu'à la fin de la ligne (facultatif)|
|  ::titre:: |                         Titre de la question (facultatif)|
| texte      |                        Texte de la question (devient titre si aucun titre n'est spécifié) |
|  [...format...]   |              Le format du morceau de texte suivant. Les options sont [html], [moodle], [plain] et [markdown]. La valeur par défaut est [moodle] pour le texte de la question, les autres parties de la question utilisent par défaut le format utilisé pour le texte de la question.|
|  {   |                               Commencer la ou les réponses : sans aucune réponse, le texte est une description des questions suivantes |
|  {T} ou {F}        |                 Réponse Vrai ou Faux ; également {TRUE} et {FALSE} |
|  { ... = c'est vrai ... } |       Bonne réponse pour choix multiple, (réponse multiple ? -- voir commentaires de la page) ou remplissez le blanc |
|  { ... ~faux ... }     |          Réponse incorrecte pour choix multiple ou réponse multiple |
|  { ... =élément -> correspondance ... } |  Réponse aux questions correspondantes|
|  #texte de rétroaction      |        Répondez aux commentaires pour les réponses multiples, à compléter ou numériques précédentes |
|  ####commentaires généraux   |       Commentaires généraux |
|  {#                          |       Commencer la ou les réponses numériques |
|  réponse : tolérance         |       Réponse numérique acceptée dans la plage de tolérance de ± |
| ValeurMin..ValeurMax                    |      Valeurs inférieures et supérieures de la réponse numérique acceptée |
|  =%n%réponse:tolérance       |      n pourcentage de crédit pour l'une des multiples plages numériques dans la tolérance de la réponse |
|  }                           |       Fin des réponses |
|  \caractère                |      La barre oblique inverse échappe à la signification particulière de \~, =, #, {, } et : |
|  \n                          |      Place une nouvelle ligne dans le texte de la question car des lignes vides délimitent les questions |


### Quelques exemples
```gift
// vrai/faux  
::Q1-Nom de la question::   
  1+1=2 {T}  


// choix multiples avec retour spécifié pour les bonnes et les mauvaises
réponses
::Q2-Nom de la question:: Qu'y a-t-il entre l'orange et le vert dans le spectre ?  
{ 
  =jaune # à droite ; Bravo !  
  ~rouge # faux, c'est jaune
  ~bleu # faux, c'est jaune 
}

// remplissez le blanc
::Q3-Nom de la question:: Deux plus {=deux =2} est égal à quatre.

// Correpondance
::Q4-Nom de la question:: 
Quel animal mange quelle nourriture ? { 
  =chat -> nourriture pourchat 
  =chien -> nourriture pour chien 
}

// question mathématique
::Q5-Nom de la question:: Qu'est-ce qu'un nombre de 1 à 5 ? {#3:2}

// plage mathématique spécifiée avec les points de fin d'intervalle
::Q6-Nom de la question:: Qu'est-ce qu'un nombre de 1 à 5 ? {#1..5}
// traduit lors de l\'importation comme Q5, mais indisponible depuis l'interface de questions Moodle

// plusieurs réponses numériques avec crédit partiel et commentaires
::Q7-Nom de la question:: Quand Ulysses S. Grant est-il né ? {#
  =1822:0 \# Exact ! 100 % sur la note
  =%50%1822:2 \# Il est né en 1822. Un demi-crédit pour sa proximité.
}

// essai
::Q8:: Comment vas-tu ? {}

```

## Les formats de questions

Il existe plusieurs façons d'utiliser un éditeur de texte pour écrire un format GIFT. Nous essaierons de montrer la version simple par exemple et dans certains formats nous introduirons des fonctionnalités plus complexes qui peuvent être importées dans de nombreux formats de questions Moodle.

### Choix multiple
- **Question standard**

```GIFT
//Ligne de commentaire
::Titre de la question 
:: Question {
=Une bonne réponse
~Mauvaise réponse1 #Texte de feedback (Rétroaction) à une mauvaise réponse1
~Mauvaise réponse2 #Texte de feedback (Rétroaction) à une mauvaise réponse2
~Mauvaise réponse3 #Texte de feedback (Rétroaction) à une mauvaise réponse3
~Mauvaise réponse4 #Texte de feedback (Rétroaction) à une mauvaise réponse4
}
```
Le format le plus court pour une question à choix multiples est :
```GIFT
Question{= Une bonne réponse \~ Mauvaise réponse 1 \~ Mauvaise réponse 2
\~ Mauvaise réponse 3 \~ Mauvaise réponse 4 }
```
!!! tips "Astuce" 
    - En insérant un numéro dans le **Titre** de la question, les questions se rangent dans l'**ordre** lors de l'import.
    - Si vous ne spécifiez pas de titre de question, la question **entière** sera utilisée comme titre au moment de l'importation dans Moodle.


- **Question difficile**  

Pour les questions difficiles, on peut attribuer des points à questions presque juste {~mauvaise réponse ~%50%réponse à moitié crédit =réponse à crédit complet}.

```GIFT
// Commentaire
::Q1-Titre de la question:: 
::La ville natale de Jésus::Jésus-Christ était originaire de {
~Jérusalem #C'était une ville importante, mais la mauvaise réponse.
~%25%Bethléem #Il est né ici, mais n'a pas grandi ici.
~%50%Galilée #Vous devez être plus précis.
=Nazareth#Oui ! C'est exact!
}
```


### Choix multiple avec plusieurs bonnes réponses

Autrement dit, en utilisant des cases à cocher et non des boutons radio :
```GIFT
// Commentaire
::Q1-Titre de la question:: 
Quelles sont les deux personnes enterrées dans la tombe de Grant ? {
~%-100%Personne #Vous avez perdu 100 % des points à la question
~%50%Grant #Vous avez gagné 50% des points à la question
~%50%Femme de Grant #Vous avez gagné 50% des points à la question
~%-100%Père de Grant #Vous avez perdu 100 % des points à la question
}
```


### Vrai faux

Dans ce type de question, la réponse indique si la déclaration est vraie
ou fausse. La réponse doit être écrite sous la forme {TRUE} ou {FALSE},
ou abrégée en {T} ou {F}.
```GIFT
// Commentaire
::Q1-Titre de la question:: 
Le soleil se lève à l'est. {T}
```

### Réponse courte

Les réponses aux questions de type réponse courte sont toutes préfixées
par un signe égal (=), indiquant qu'elles sont toutes des réponses
correctes. Les réponses ne doivent pas contenir de tilde.

Voici deux exemples utilisant la méthode simple montrant les bonnes
réponses possibles pour le crédit.
```GIFT
::Q1-Titre de la question::
Quelle est la capitale de la Belgique ?{=Bruxelles =Bruxelle =Bruxel}

::Q2-Titre de la question::
Deux plus deux égale {=quatre =4}
```
Normalement, votre moodle n'est pas sensible à la casse, c'est à dire aux minuscules, majuscules.

### Correspondances

Les paires correspondantes commencent par un signe égal (=) et sont
séparées par ce symbole `->`. Il doit y avoir au moins **trois paires**
correspondantes.

```GIFT
::Q1-Titre de la question::
Associez les pays suivants avec leurs capitales correspondantes. {
=Canada -> Ottawa
=Italie -> Rome
=Japon -> Tokyo
=Inde -> New Delhi
}
```
Les questions correspondantes **ne prennent pas en charge les commentaires** ou les pondérations de réponse en pourcentage.

### Mot manquant

Le format Mot manquant insère automatiquement une ligne à remplir (comme ceci _____) au milieu de la phrase. Pour utiliser le format Mot manquant, placez les réponses à l\'endroit où vous souhaitez que la ligne apparaisse dans la phrase.
```GIFT
::Q1-Titre de la question::
Moodle coûte {~beaucoup d'argent =rien ~une petite somme} à
télécharger depuis moodle.org.
```

### Questions numériques

La section de réponses aux questions numériques doit commencer par un signe dièse (#). Les réponses numériques peuvent inclure une marge d'erreur, qui est écrite après la bonne réponse, séparée par deux points. Ainsi, par exemple, si la bonne réponse est comprise entre 1,5 et 2,5, elle s'écrira comme suit {#2:0,5}. Cela indique que 2 avec une marge d'erreur de 0,5 est correct (c'est-à-dire l'étendue de 1,5 à 2,5). Si aucune marge d'erreur n'est spécifiée, elle sera considérée comme nulle.

- **La fourchette**   
Voici une question simple au format numérique. Il acceptera une fourchette de 5 ans.

```GIFT
::Q1-Titre de la question::
Quand Ulysses S. Grant est-il né ?{#1822:5}
```

- **La marge**  
C'est une bonne idée de vérifier les marges de la plage, 3,141 n'est pas considéré comme correct et 3,142 est considéré dans la plage.

```GIFT
::Q1-Titre de la question::
Quelle est la valeur de pi (à 3 décimales près) ? {#3.14159:0.0005}.
```

- **Valeur min, valeur max**  
Facultativement, les réponses numériques peuvent être écrites sous forme d'étendue au format suivant {MinimumValue..MaximumValue}.

```GIFT
::Q1-Titre de la question::
Quelle est la valeur de pi (à 3 décimales près) ? {#3.141..3.142}.
```

- **Plusieurs réponses**   
L'interface du navigateur de Moodle ne prend pas en charge plusieurs réponses numériques, mais le code de Moodle le peut, tout comme GIFT.
Cela peut être utilisé pour spécifier plusieurs portées numériques et peut être particulièrement utile lorsqu'il est combiné avec des pourcentages de poids. Si plusieurs réponses sont utilisées, elles doivent être séparées par un signe égal, comme pour les questions à réponse courte.

```GIFT
::Q1-Titre de la question::
Quand Ulysses S. Grant est-il né? {#
=1822:0
=%50%1822:2
}
```

### Essai

Une question à développement est simplement une question avec un champ de réponse vide. Rien n'est autorisé entre les accolades.

```GIFT
::Q1-Titre de la question::
Écrivez une courte biographie de Victor Hugo. {}
```

### Description

Une description "question" n'a aucune partie réponse
```GIFT
Vous pouvez utiliser votre calculatrice et votre papier pour ces prochaines questions mathématiques.
```

## Possibilités

En plus de ces types de questions de base, ce filtre propose les options
suivantes : commentaires de ligne, nom de la question, commentaires et
pourcentage de poids de réponse.

### Formatage de texte, les unités en puissance.
Pour formater le texte, le mettre en gras, souligné, mettre les unités en puissance ou insérer un lien hypertexte, on peut utiliser le format [Markdown](https://www.linuxadictos.com/fr/Le-d%C3%A9marque-est-probablement-le-meilleur-moyen-de-prendre-des-notes-personnelles%2C-nous-vous-expliquons-comment-l%27utiliser.html) ou html.

```GIFT
::Q1-Titre de la question::
[markdown]La *fête américaine de Thanksgiving* est célébrée le {
~deuxième
~troisième
=quatrième
} jeudi de novembre.
```
```GIFT
::Q2-Titre de la question::
[html]Quelle la surface d'un rectangle de 6 m de largeur et de 3 m de longueur {
~18 m<sup>2</sup>
~18 m<sup>3</sup>
=18 m<sup>4</sup>
}
```

### Caractères spéciaux
Ces symboles `\~ = \# { } :` contrôlent le fonctionnement de ce filtre et ne peuvent pas être utilisés comme texte normal dans les questions. Ces symboles ayant un rôle particulier dans la détermination du fonctionnement de ce filtre, ils sont appelés « caractères de contrôle ». Mais parfois, vous souhaiterez peut-être utiliser l'un de ces caractères, par exemple pour afficher une formule mathématique dans une question. La façon de contourner ce problème consiste à "échapper" aux caractères de contrôle. Cela signifie simplement mettre une barre
oblique inverse (\\) avant un caractère de contrôle afin que le filtre ache que vous souhaitez l'utiliser comme caractère littéral plutôt que comme caractère de contrôle. Par exemple:

```GIFT
::Q1-Titre de la question::
Quelle réponse est égale à 5 ? {
~ \= 2 + 2
= \= 2 + 3
~ \= 2 + 4
}
```


### Spécification des catégories

Il est possible de changer la catégorie dans laquelle les questions sont ajoutées au sein du fichier GIFT. Vous pouvez changer de catégorie autant de fois que vous le souhaitez au sein du fichier. Toutes les questions après le modificateur jusqu\'au modificateur suivant ou à la fin du fichier seront ajoutées à la catégorie spécifiée. Jusqu'au premier modificateur de catégorie, la catégorie spécifiée sur l'écran d'importation sera utilisée. Notez que pour que cela fonctionne, la case from file: doit être cochée sur l'écran d'importation.
Pour inclure un modificateur de catégorie, incluez une ligne comme celle-ci (avec une ligne vide avant et après) :

`$CATEGORIE : tom/dick/harry`
ou simplement
`$CATEGORY : MaCategorie`

..le premier exemple spécifie un chemin de catégories imbriquées. Dans ce cas, les questions iront à Harry. Les catégories sont créées si elles n'existent pas.

Pour savoir comment vos catégories sont organisées, vous pouvez d'abord essayer d'exporter certaines questions, y compris les données de catégorie, et vérifier le fichier au format GIFT exporté.


## Liens
- [https://assiniboine.net - fichier pdf](https://https://assiniboine.net/sites/default/files/2020-03/Moodle_GIFT_Format_Moodle.pdf/sites/default/files/2020-03/Moodle_GIFT_Format_Moodle.pdf)
- [Gift-reference](http://buypct.com/gift_reference.pdf)
- [elearnmagazine.com](https://www.elearnmagazine.com/howto/gift-format-moodle-quiz-scripting/)
- [mi.ucam.ac.ma](http://www.mi.ucam.ac.ma/c2i/help.php?module=quiz&file=formatgift.html)