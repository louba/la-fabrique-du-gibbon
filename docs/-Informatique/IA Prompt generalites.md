---
title: Le prompt pour l'IA
summary: Comment générer un bon prompt pour l'IA.
authors:
    - Vincent
date:
#   created: 2023-12-31
#   updated: 2024-01-02
  icon: material/home
  locale: fr
---
# Comment créer des prompts efficaces pour les LLM ?

Les grands modèles de langage (LLM) sont des outils puissants qui permettent d'accomplir diverses tâches allant de la rédaction de contenu à l'analyse de données en passant par le codage. Cependant, pour profiter pleinement de leur potentiel, il est essentiel de formuler des prompts clairs et structurés. Voici comment y parvenir en décomposant le **prompt** :

1. **Le persona** : Définissez la personnalité, le style ou le rôle que le LLM doit adopter. Par exemple, un expert en événementiel, un poète romantique ou un conseiller financier.

2. **La tâche** : Précisez clairement ce que vous attendez du LLM, comme rédiger un article, analyser des données ou générer des idées créatives.

3. **Le contexte** : Fournissez les informations de base pertinentes pour la tâche, comme le sujet, le public cible ou les contraintes spécifiques.

4. **Le format** : Indiquez la structure souhaitée pour la sortie, par exemple un article de blog, une présentation PowerPoint ou du code Python.

5. **Le ton** : Spécifiez le style de communication désiré, comme formel, amical, humoristique ou motivant.

Exemple de prompt pour un anniversaire pour 100 amis d'une personne de 20 ans :

- Persona : Tu es un organisateur d'événements créatif et énergique.  
- Tâche : Ta tâche est de proposer des idées d'activités, de décorations et de menus originaux pour une fête d'anniversaire géante.  
- Contexte : La fête aura lieu dans un grand jardin et réunira environ 100 jeunes adultes âgés de 20 ans.  Je souhaite un événement mémorable et amusant qui sortira de l'ordinaire.  
- Format : Une liste détaillée d'idées réparties en différentes sections (activités, décorations, menus, etc.)  
- Ton : Enthousiaste, créatif et adapté à un public jeune et branché.  

En fournissant ces informations structurées, vous aiderez le LLM à mieux comprendre vos attentes et à générer une sortie pertinente et de qualité. N'hésitez pas à ajuster les différents éléments en fonction de vos besoins, **vous n'avez pas besoin de toutes les parties à chaque prompt**.